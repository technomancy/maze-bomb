local lume = require "lume"
local x = 30
local y = 573
local size = 10
local speed = 100  

bomb_img = love.graphics.newImage("bomb.png")
local bombs = {}
local make_bomb = function()
  table.insert(bombs, {x = x, y = y, timer = 3, radius = 10})
end

local levels = {}
local level = 2

levels[1] = { { 0, 0, 10, love.graphics.getHeight()},
  { 0, 0, love.graphics.getWidth(), 10},
  {  love.graphics.getWidth() - 9, 0, 10, love.graphics.getHeight()},
  {  0,  love.graphics.getHeight() - 9,  love.graphics.getWidth(), 10}, 
  {100, 100, 100, 9 },
  {200, 200, 405, 9},
  {100, 190 + 10, 9, 490},
  {200, 100, 10, 500},
  {119, 49, 300, 10},
  {100, 49, 20, 100},
  {700, 107.1466008814, 25, 10},
  {541, 22, 10, 98},
  {542, 11, 8, 12},
  {595, 200, 10, 290}}

local goal = {x = 395, y = 268, w = 5,h = 5}

local Gw = {}
Gw[1] = {{211, 347, 382, 44},
  {401, 10, 12, 37},
  {595, 492, 15, 98},
  {550, 122, 7, 75},
  {604, 201, 183, 25},
  
}
Gw[2] = {{700, 510, 81, 40},
   {495, 0, 23, 525},
   {403, 518, 17, 205},
   }

levels[2] = {
  {1, 140, 10, 690},
  {3, 140, 14, 139},
  {13, 520, 403, 17},
  {42, 591, 783, 4},
  {419, 524, 287, 14},
  {578, 586, 59, 190
  },
  {441, 521, 7, -263}
}

local probloms = {"new levels",}

table.remove(probloms,2)
for i = 1, #probloms do
  print(probloms[i])
end
function check(x1,y1,w1,h1, x2,y2,w2,h2)
  return x1 < x2+w2 and
  x2 < x1+w1 and
  y1 < y2+h2 and
  y2 < y1+h1
end

local function collided_any_wall(x, y)
  for i, wall in pairs(levels[level]) do
    if check(x - size, y - size, size * 2, size * 2, wall[1], wall[2], wall[3], wall[4]) then
      return true
    end
  end
  for i, wall in pairs(Gw[level]) do
    if check(x - size, y - size, size * 2, size * 2, wall[1], wall[2], wall[3], wall[4]) then
      return true
    end
  end
  return false
end

function update_bomb(bomb, dt)
  bomb.timer = bomb.timer - dt
  if bomb.timer < -3 then
    lume.remove(bombs, bomb)
  elseif bomb.timer < 0 then
    bomb.radius = bomb.radius + dt * 10
    for i, wall in pairs(Gw[level]) do
      if check(bomb.x - bomb.radius, bomb.y - bomb.radius, bomb.radius*2,
               bomb.radius*2, wall[1], wall[2], wall[3], wall[4]) then
        table.remove(Gw[level], i)
      end
    end
  end
end

local next_level = function()
  max = #levels
  if(level < max) then
    level = level + 1
    x = 30
    y = 573
  end
end

love.keyreleased = function(key)
  if(key == "n") then next_level() end
end

function love.update(dt)
  local old_x, old_y = x, y
  if love.keyboard.isDown("right")then
    x = x + speed * dt
  end
  if love.keyboard.isDown("left")then
    x = x - speed * dt
  end
  if love.keyboard.isDown("down")then
    y = y + speed * dt
  end
  if love.keyboard.isDown("up")then
    y = y - speed * dt
  end
  if love.keyboard.isDown("a") then
    print(x, y)
  end
  if love.keyboard.isDown("escape") then
    love.event.quit()
  end
  if collided_any_wall(x, y) then
    x, y = old_x, old_y
  end
  if love.keyboard.isDown("d")then
    make_bomb()
  end
  for _, bomb in pairs(bombs) do
    update_bomb(bomb, dt)
  end
end

function love.draw()
  love.graphics.setColor(255,255,255)
  for _, wall in pairs(levels[level]) do
    love.graphics.rectangle("fill", wall[1], wall[2], wall[3], wall[4])
  end
  love.graphics.setColor(43, 237, 21)
  for _, w in pairs(Gw[level]) do
    love.graphics.rectangle("fill", w[1], w[2], w[3], w[4])
  end
  love.graphics.setColor(255, 255, 255)
  for _, bomb in pairs(bombs) do
    if bomb.timer > 0 then
      love.graphics.draw(bomb_img,bomb.x,bomb.y)
    else
      love.graphics.circle("fill", bomb.x, bomb.y, bomb.radius)
    end
  end
-- winning place
  love.graphics.setColor(142,138,3)
  love.graphics.rectangle("fill",goal.x, goal.y, goal.w, goal.h)
-- player
  love.graphics.circle( "fill", x, y, size)
  if check(x, y, size, size, goal.x, goal.y, goal.w, goal.h) then
    --change to level 2
    next_level()
  end
end

function love.mousepressed( x, y, button )
  pressed_x, pressed_y = x,y
end

function love.mousereleased( x, y, button )
  local w, h = x-pressed_x, y-pressed_y
  print(string.format("{%s, %s, %s, %s}", pressed_x, pressed_y, w, h))
end
<<<<<<< HEAD
=======

function love.load()
   if arg[#arg] == "-debug" then require("mobdebug").start() end
end
>>>>>>> more walls
